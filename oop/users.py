class User:

    def __init__(self, email, gender):
        self.email = email
        self.gender = gender
        self.usage_time = 0

    def use_aplication(self, usage_time_in_seconds):
        self.usage_time += usage_time_in_seconds

if __name__ == '__main__':
    user_kate = User('katekate@exampel.com', 'female')
    user_james = User('james@example.com', 'male')
    print(user_kate.email)
    print(user_james.email)
    user_kate.email = 'kate_new_example.com'
    print('Kate new email:', user_kate.email)
    print(user_kate.usage_time)
    print(user_james.usage_time)
    user_kate.use_aplication(100)
    print(user_kate.usage_time)
    print(user_james.usage_time)
