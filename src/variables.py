first_name = "michał"
last_name = "Kosmala"
age = 25

print(first_name)
print(last_name)
print(age)

# describes my friend's variables
name = "Tomek"
age_in_years = 38
numbers_of_animals = 2
he_have_a_driving_licence = False
how_long_we_known_each_other_in_years = 9.5
print(name, age_in_years, numbers_of_animals, he_have_a_driving_licence, how_long_we_known_each_other_in_years, sep=',')
