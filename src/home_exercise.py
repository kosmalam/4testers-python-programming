import random

female_fnames = ['Kate', 'Agnieszka', 'Anna', 'Maria', 'Joss', 'Eryka']
male_fnames = ['James', 'Bob', 'Jan', 'Hans', 'Orestes', 'Saturnin']
surnames = ['Smith', 'Kowalski', 'Yu', 'Bona', 'Muster', 'Skinner', 'Cox', 'Brick', 'Malina']
countries = ['Poland', 'United Kingdom', 'Germany', 'France', 'Other']


def print_random_female_name(female_fnames):
    random_female_firstname = random.choice(female_fnames)
    print(random_female_firstname)


def print_random_male_name(male_fnames):
    random_male_firstname = random.choice(male_fnames)
    print(random_male_firstname)


def generate_random_email():
    domain = "example.com"
    names = (female_fnames, male_fnames)
    random_name = random.choice(names)
    return f'{random_name}@{domain}'


def print_random_age():
    random_age = random.randint(1, 65)
    print(random_age)

def get_dictionary():
    new_dictionary = []



if __name__ == '__main__':
    print_random_female_name(female_fnames)
    print_random_male_name(male_fnames)
    print_random_age()
    print(generate_random_email)
