import string
import random
from typing import Dict, Union


# Zadanie 1

def calculate_average_of_numbers(input_list):
    return sum(input_list) / len(input_list)


def average_of_two_numbers(a, b):
    return (a + b) / 2


# Zadanie 2

def generate_random_password():
    characters = string.ascii_letters + string.digits + string.punctuation
    return ''.join(random.choice(characters) for i in range(8))


def gerate_login_data(email_address):
    generated_password = generate_random_password()
    return {"email": email_address, "password": generated_password}


# Homework

def calculate_player_level(exp_points):
    return int(exp_points / 1000)
    # return exp_points // 1000


def print_description_of_the_player(player):
    nick = player['nick']
    type = player['type']
    exp_points = player['exp_points']
    level = calculate_player_level(exp_points)
    print(f'The player "{nick}" is of type {type.capitalize()} and has {exp_points} EXP. The player is on level {level}.')


if __name__ == '__main__':
    january = [-4, 1.0, -7, 2]
    february = [-13, -9, -3, 3]
    january_average = calculate_average_of_numbers(january)
    february_average = calculate_average_of_numbers(february)
    bimonthly_average = average_of_two_numbers(january_average, february_average)
    print(bimonthly_average)

    print(gerate_login_data("example@com.eu"))
    print(gerate_login_data("gucci@guddi.com"))

    player = {
        'nick': 'maestro_54',
        'type': 'warrior',
        'exp_points': 3000,
    }

    player2 = {
        'nick': 'andor_55',
        'type': 'magican',
        'exp_points': 4500,
    }

    gererate_player_descripton = print_description_of_the_player(player)
    gererate_player_descripton = print_description_of_the_player(player2)
