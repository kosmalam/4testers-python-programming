def display_speed_information(speed):
    # if speed <= 50:
    #   print('Thank you, your speed is below limit! :)')
    # else:
    #    print('Slow down')
    if speed > 50:
        print('Slow down!')
    else:
        print('Thank you, your speed is below limit!')


def check_temrature_and_presure_condition(temperature, pressure):
    if temperature == 0 and pressure == 1013:
        return True
    else:
        return False


def calculate_fine_amount(speed):
    return 500 + (speed - 50) * 10


def print_the_value_of_speeding_fine_in_bulit_up_area(speed):
    print('your speed was', speed)
    if speed > 100:
        print('you just lost your driving license !')
    elif speed > 50:
        print(f'You just got a fine! Fine amount {calculate_fine_amount(speed)}')
    else:
        print('Your speed is fine')


def get_grade_info(grade):
    if not (isinstance(grade, float) or isinstance(grade, int)):  # zadanie z gwiazdką :)
        return 'N/A'
    elif grade < 2 or grade > 5:
        return 'N/A'
    elif grade >= 4.5:
        return 'Bardzo dobry'
    elif grade >= 4:
        return 'dobry'
    elif grade >= 3.0:
        return 'dostateczny'
    else:
        return 'niedosteteczny'


if __name__ == '__main__':
    display_speed_information(50)
    display_speed_information(49)
    display_speed_information(51)

    print(check_temrature_and_presure_condition(0, 1013))
    print(check_temrature_and_presure_condition(1, 1013))
    print(check_temrature_and_presure_condition(0, 1014))
    print(check_temrature_and_presure_condition(1, 1014))

    print_the_value_of_speeding_fine_in_bulit_up_area(101)
    print_the_value_of_speeding_fine_in_bulit_up_area(100)
    print_the_value_of_speeding_fine_in_bulit_up_area(49)
    print_the_value_of_speeding_fine_in_bulit_up_area(50)
    print_the_value_of_speeding_fine_in_bulit_up_area(51)

    print(get_grade_info(5))
    print(get_grade_info(4.5))
    print(get_grade_info(4))
    print(get_grade_info(3))
    print(get_grade_info(2))
    print(get_grade_info(1))
