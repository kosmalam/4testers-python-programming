movies = ["Star Wars", "Dune", "Alien", "Prometheus", "lord of the Rings"]
last_movie = movies[-1]
movies.append("Alien 2")
print(len(movies))
middle_movies = movies[2:5]
print(middle_movies)

movies.insert(0, "Top Gun")
print(movies)

emails = ["a@example.com", "b@example.com"]
print(len(emails))
first_email = emails[0]
print(first_email)
last_email = emails[-1]
print(last_email)
emails.insert(2, "cde@example.com")
print(emails)

friend = {
    'name' : 'Tomek',
    'age' : 35,
    'hobby' : ['running', 'bike']
}
friend_hobbies = friend['hobby']
print("Hobbies of my friend:", friend_hobbies)
print(f"my friend has {len(friend_hobbies)} hobbies")
friend['hobby'].append('football')
print(friend)
friend['married'] = True
friend['age'] = 44
print(friend)
