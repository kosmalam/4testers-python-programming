import random
import time


def generate_random_number():
    return random.randint(0, 40)


def generate_random_email():
    domain = 'example.com'
    name_list = ['james', 'weronika', 'kate', 'bill', 'alfred', 'juniko']
     # suffix = random.randit(1, 1_000_000)
    suffix = time.time()
    return f'{random.choice(name_list)}.{suffix}@{(domain)}'


def generate_random_boolean():
    return random.choice([True, False])


def get_dictionary_with_random_personal_data():
    random_email = generate_random_email()
    random_number = generate_random_number()
    random_boolean = generate_random_boolean()
    return {
        "email": random_email,
        "seniority_years": random_number,
        "female": random_boolean
    }


def get_list_of_dictionaries_random_personal_data(number_of_dictionaries):
    list_of_dictionaries = []
    for number in range(number_of_dictionaries):
        list_of_dictionaries.append(get_dictionary_with_random_personal_data())
    return list_of_dictionaries


def get_emails_of_workers_with_seniority_years_above_10(list_of_employs):
    senior_employees = []
    for employee in list_of_employs:
        if employee["seniority_years"] > 10:
            senior_employees.append(employee["email"])
    return senior_employees

if __name__ == '__main__':
    print(get_dictionary_with_random_personal_data())
    print(get_list_of_dictionaries_random_personal_data(10))
    test_employee_list = get_list_of_dictionaries_random_personal_data(20)
    print(test_employee_list)
    test_senior_employee_emails = get_emails_of_workers_with_seniority_years_above_10(test_employee_list)
    print(test_senior_employee_emails)
