animals = [
    {'name': 'Burek', 'kind': 'dog', 'age': 7},
    {'name': 'Bonifacy', 'kind': 'cat', 'age': None},
    {'name': 'Misio', 'kind': 'hamster', 'age': 1},
    {'name': 'Muppet', 'kind': 'dog', 'age': 7},
]

adresses = [
    {'city': 'Warszawa', 'street': 'Maczka', 'house_number': '3', 'post_code': '01-864'},
    {'city': 'Poznań', 'street': 'Powstańsów Wielkopolskich', 'house_number': '23', 'post_code': '10-654'},
    {'city': 'Kraków', 'street': 'Chopina', 'house_number': '45', 'post_code': '02-234'},
]
print(adresses[-1]['post_code'])
print(adresses[1]['city'])
adresses[0]['street'] = 'Legii Cudzoziemskiej'
print(adresses)
print(animals[-1]['name'])
animals[1]['age'] = 2
animals.append({'name': 'Reksio', 'kind': 'dog', 'age': 9})
print(animals)
