from datetime import date


class Car:
    def __init__(self, model, car_year):
        self.model = model
        self.car_year = car_year
        self.mileage = 0

    def set_mileage_of_the_car(self, distance):
        self.mileage += distance
        return self.mileage

    def set_car_age(self, car_year):
        return date.today().year - car_year

    def has_car_warranty(self):
        age_of_car_in_year = date.today().year - int(self.car_year)
        print(age_of_car_in_year)
        if age_of_car_in_year >= 7 or int(self.mileage) >= 120000:
            return False
        else:
            return True

    def get_description(self):
        return f'This is a {self.model} made in {self.car_year}. Currently it drove {self.mileage}'


if __name__ == '__main__':
    car1 = Car('Honda Jazz', '1990')
    car2 = Car('Opel Astra', '2001')
    car3 = Car('Toyota Auris', '2020')

    car1.set_mileage_of_the_car(250000)
    car2.set_mileage_of_the_car(190000)
    car3.set_mileage_of_the_car(50000)
    car1.set_mileage_of_the_car(100000)
    print(car1.has_car_warranty())

    print(car1.get_description())
    print(car2.get_description())
    print(car3.get_description())
