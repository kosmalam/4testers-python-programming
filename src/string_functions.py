def print_greetings_for_a_person_in_the_city( person_name, city):
    print(f"Witaj{person_name}! Miło Cię widzieć w naszym mieście: {city}!")


def email_adress( name, surname, domain):
    print (f"{name.lower()}.{surname.lower()}@{domain}")


if __name__ == '__main__':
    print_greetings_for_a_person_in_the_city( "Michał", "Warszawa")
    email_adress("Janusz", "Nowak", "4testers.pl")
    email_adress("Barbara", "Kowalska", "4testers.pl")









