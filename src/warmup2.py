# Zadanie 1
def calculate_square(number):
    return number ** 2


# Zadanie 2
def celsius_to_fahrenheit(temp_celsius):
    return 1.8 * temp_celsius + 32


# Zadanie 3
def volume_of_the_cuboid(a, b, c):
    return a * b * c


if __name__ == '__main__':
    # Zadanie 1
    square_1 = calculate_square(0)
    square_2 = calculate_square(16)
    square_3 = calculate_square(2.55)

    print(square_1)
    print(square_2)
    print(square_3)

    # Zadanie 2
    farenheit_tem = celsius_to_fahrenheit(20)
    print("20 stopni Celcjusza to:", farenheit_tem, "Stopni F")

    # Zadanie 3
    cuboid = volume_of_the_cuboid(3, 5, 7)
    print(cuboid)
