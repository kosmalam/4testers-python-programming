from src.car import set_mileage_of_the_car
from src.car import set_car_age
from src.car import has_car_warranty
from src.car import get_description

def test_new_car_has_0_distance():
    test_car = Car(0)
    test_car.set_mileage_of_the_car(0)
    assert test_car.mileage == 0

def test_car_has_100km_distance_after_drive_100km():
    test_car = Car(0)
    test_car.set_mileage_of_the_car(100)
    assert test_car.mileage == 100
